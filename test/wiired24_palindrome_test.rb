require "test_helper"

class Wiired24PalindromeTest < Minitest::Test
 
  def test_for_non_palindrome
    refute "apple".palindrome?
  end

  def test_for_mixed_case_palindrome
    assert "RaCecar".palindrome?
end

def test_for_palindrome_with_punctuation
    assert "Madam I'm Adam".palindrome?
end

def test_interger_palindrome
  assert 12321.palindrome?
end

end
