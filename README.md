# Wiired24Palindrome

This is a simple Ruby Gem that you can add to any of your programs. It
will detect if there is or isn't a palindrome when supplied with
a given phrase.


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'wiired24_palindrome'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install wiired24_palindrome

## Usage

It's as simple as running:
    "tacocat".palindrome?


## Contributing

Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/[wiired24]/wiired24_palindrome. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct]


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).


